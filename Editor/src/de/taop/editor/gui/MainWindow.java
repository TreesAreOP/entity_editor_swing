package de.taop.editor.gui;
import com.badlogic.gdx.backends.lwjgl.LwjglCanvas;
import de.taop.editor.gdx.*;
import de.taop.editor.gdx.Renderer;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Adrian on 10.09.2016.
 */
public class MainWindow {
    private JPanel mainPanel;
    private JPanel previewPanel;

    public static void main(String[] args) {
        for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
                try {
                    UIManager.setLookAndFeel(info.getClassName());
                } catch (Throwable ignored) {
                }
                break;
            }
        }
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame("MainWindow");

                MainWindow window = new MainWindow();

                frame.setContentPane(window.mainPanel);
                frame.setSize(800, 650);
                frame.setLocationRelativeTo(null);
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                frame.setVisible(true);


            }
        });
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        previewPanel = new JPanel(new BorderLayout());
        previewPanel.add(new LwjglCanvas(new Renderer()).getCanvas());
        previewPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 4));
    }
}
