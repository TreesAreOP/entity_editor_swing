package de.taop.editor.gdx;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by Adrian on 10.09.2016.
 */
public class Renderer implements ApplicationListener, InputProcessor {

    private ShapeRenderer debug;
    private SpriteBatch batch;

    private OrthographicCamera cam;

    public void create() {
        if (batch != null) return;

        batch = new SpriteBatch();
        debug = new ShapeRenderer();

        cam = new OrthographicCamera();

        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void resize(int width, int height) {
        Gdx.gl.glViewport(0, 0, width, height);

        cam.setToOrtho(false, width, height);
        cam.update();
    }

    @Override
    public void render() {
        int viewWidth = Gdx.graphics.getWidth();
        int viewHeight = Gdx.graphics.getHeight();

        float delta = Gdx.graphics.getDeltaTime();

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        debug.setProjectionMatrix(cam.combined);
        batch.setProjectionMatrix(cam.combined);

        debug.begin(ShapeRenderer.ShapeType.Line);

        debug.setColor(Color.RED);

        debug.rect(0, 0, 20, 20);
        debug.end();


    }

    @Override
    public void dispose() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int newParam) {
        return false;
    }

    @Override
    public boolean touchUp(int x, int y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int x, int y, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int x, int y) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}